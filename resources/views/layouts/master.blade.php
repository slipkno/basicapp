<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>BasicApp | @yield('title')</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <style type="text/css">
        body{
            padding-top: 70px;
        }
    </style>
</head>
<body>

<nav role="navigation" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="{{ active_class(if_route(['all-users'])) }}"><a href="{{ URL::to('all-users') }}">All Users</a></li>
                <li class="{{ active_class(if_route(['register-new-user'])) }}">
                    @if(!in_array(Session::get('user')['roles'], ['register']))
                        <a href="{{ URL::to('register-new-user') }}">Register New User</a>
                    @else
                        <a href="#">Register New User</a>
                    @endif
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ URL::to('logout') }}">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row text-center">
        <h4>Hello, <b class="text-warning">{{ Session::get('user')['name'] }}</b></h4>
        <hr>
    </div>

    <div class="content">
        @yield('content')
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
</body>
</html>
