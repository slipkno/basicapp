@extends('layouts.master')

@section('title', 'All Users')

@section('content')

    <div class="row">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>Username</td>
                    <td>First Name</td>
                    <td>Last Name</td>
                    <td>Email Address</td>
                    <td>Type</td>
                    <td>Roles</td>
                    <td>Status</td>
                </tr>
            </thead>
            <tbody>
            @if($paginator)
                @foreach($paginator->items() as $userData)
                    {!! Form::open() !!}
                    {!! Form::hidden('user_id', $userData->id) !!}
                        <tr>
                            <td>{{ $userData->username }}</td>
                            <td>{{ $userData->first_name }}</td>
                            <td>{{ $userData->last_name }}</td>
                            <td>{{ $userData->email }}</td>
                            <td>
                                @if(!in_array(Session::get('user')['roles'], ['update']))
                                    <label class="select" onchange="this.form.submit()">
                                        <select name="type_id">
                                            @foreach($types as $type)
                                                <option value="{{ $type->id }}" {{ ($userData->type_id == $type->id) ? "selected" : "" }}>{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                    </label>
                                @else
                                    {{ $userData->type_name }}
                                @endif
                            </td>
                            <td>
                                {!! Form::submit('Roles', ['name' => 'roles', 'class' => 'btn btn-xs btn-default']) !!}
                            </td>
                            <td>
                                @if(!in_array(Session::get('user')['roles'], ['block']))
                                    @if($userData->is_active == 1)
                                        {!! Form::submit('Deactivate', ['name' => 'deactivate', 'class' => 'btn btn-xs btn-danger']) !!}
                                    @else
                                        {!! Form::submit('Active', ['name' => 'activate', 'class' => 'btn btn-xs btn-success']) !!}
                                    @endif
                                @else
                                    @if($userData->is_active == 1)
                                        {!! Form::submit('Deactivate', ['name' => 'deactivate', 'class' => 'btn btn-xs btn-danger disabled']) !!}
                                    @else
                                        {!! Form::submit('Active', ['name' => 'activate', 'class' => 'btn btn-xs btn-success disabled']) !!}
                                    @endif
                                @endif
                            </td>
                        </tr>
                    {!! Form::close() !!}
                @endforeach
            @else
                <tr>
                    <td colspan="7">No Users Yet</td>
                </tr>
            @endif
            </tbody>
        </table>
        @if($paginator)
            <div class="pager">
                {!! $paginator->render() !!}
            </div>
        @endif
        <div class="text-success">
            @if(Session::has('message'))
                <hr>
                <h3>{{ Session::get('message') }}</h3>
            @endif
        </div>
    </div>

@endsection
