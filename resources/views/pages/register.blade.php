@extends('layouts.master')

@section('title', 'Register New User')

@section('content')

    {!! Form::open() !!}
        <div class="form-group">
            {!! Form::label('username', 'Username', ['class' => 'input']) !!}
            {!! Form::text('username', '', ['class' => 'form-control']) !!}
            <div class="text-danger">
                {!! $errors->register->first('username') !!}
            </div>

            {!! Form::label('password', 'Password', ['class' => 'input']) !!}
            {!! Form::password('password', ['class' => 'form-control']) !!}
            <div class="text-danger">
                {!! $errors->register->first('password') !!}
            </div>

            {!! Form::label('reTypePass', 'Re-Type password', ['class' => 'input']) !!}
            {!! Form::password('reTypePass', ['class' => 'form-control']) !!}
            <div class="text-danger">
                {!! $errors->register->first('reTypePass') !!}
            </div>


            {!! Form::label('firstName', 'First Name', ['class' => 'input']) !!}
            {!! Form::text('firstName', '', ['class' => 'form-control']) !!}
            <div class="text-danger">
                {!! $errors->register->first('firstName') !!}
            </div>

            {!! Form::label('lName', 'Last Name', ['class' => 'input']) !!}
            {!! Form::text('lastName', '', ['class' => 'form-control']) !!}
            <div class="text-danger">
                {!! $errors->register->first('lastName') !!}
            </div>

            {!! Form::label('email', 'Email', ['class' => 'input']) !!}
            {!! Form::text('email', '', ['class' => 'form-control']) !!}
            <div class="text-danger">
                {!! $errors->register->first('email') !!}
            </div>

            {!! Form::label('typeId', 'Type', ['class' => 'select']) !!}
            <select name="typeId" id="typeId" class="form-control">
                @foreach($types as $type)
                    <option value="{{ $type->id }}" >{{ $type->name }}</option>
                @endforeach
            </select>

            {!! Form::label('isActive', 'User Status', ['class' => 'select']) !!}
            <select name="isActive" id="isActive" class="form-control">
                <option value="1">Active</option>
                <option value="0">Inactive</option>
            </select>

            <h3>Roles</h3>

            @foreach($roles as $role)
                {!! Form::label($role->name, $role->description, ['class' => 'checkbox']) !!}
                {!! Form::checkbox($role->name, $role->id) !!}
            @endforeach

        </div>

        <hr>

        <a href="{{ URL::to('all-users') }}" class="btn btn-default">Back</a>
        {!! Form::submit('Register New User', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}

@endsection
