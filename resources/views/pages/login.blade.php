@extends('layouts.login')

@section('title', 'Login')

@section('content')
    {{--Destroy users session--}}
    {{ Session::forget('user') }}

    <style>
        .center-block {
            width: 500px;
            height: 500px;

            position: absolute; /*it can be fixed too*/
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            margin: auto;

            /*this to solve "the content will not be cut when the window is smaller than the content": */
            max-width: 100%;
            max-height: 100%;
            overflow: auto;
        }
    </style>

    <div class="center-block text-center">
        {!! Form::open(['url' => 'login']) !!}
            <div class="form-group">
                {!! Form::label('username', 'Username') !!}
                {!! Form::text('username', '', ['class' => 'form-control']) !!}
                <div class="text-warning">
                    {!! $errors->login->first('username') !!}
                </div>

                {!! Form::label('password', 'Password') !!}
                {!! Form::password('password', ['class' => 'form-control']) !!}
                <div class="text-warning">
                    {!! $errors->login->first('password') !!}
                </div>
            </div>

            {!! Form::submit('Sign In', ['class' => 'btn btn-default']) !!}
            <div class="text-warning">
                {!! $errors->login->first('blocked') !!}
                {!! $errors->login->first('invalidUsernameOrPass') !!}
            </div>

        {!! Form::close() !!}

        <p><b class="text-warning">First configure DB</b></p>
        <p><code>DBUser: root, DBPass: ,DBTable: basic_app</code></p>
        <p><b class="text-warning">To populate with users run</b></p>
        <p><code>php artisan migrate</code></p>
        <p><b class="text-warning">Then you can use</b></p>
        <p>Usernames: user1, user2 ... user10</p>
        <p>Passwords: password</p>
    </div>



@endsection