<?php

namespace App;

use DB;

use Illuminate\Support\Facades\Session;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'users';

    protected $fillable = [
        'username',
        'password',
        'first_name',
        'last_name',
        'email',
        'type_id',
        'is_active',
    ];

    protected $hidden = [
        'password',
    ];

    public function type()
    {
        return $this->hasOne('App\Type');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    // Helper function to paginate
    public static function getAllUsers()
    {
        // Hide Logged in User form results
        $ids[0] = Session::get('user')['id'];

        return DB::table('users')
            ->join('types', 'users.type_id', '=', 'types.id')
            ->select('users.*', 'types.name as type_name')
            ->whereNotIn('users.id', $ids)
            ->orderBy('users.updated_at', 'desc');
    }
}
