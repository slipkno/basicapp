<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterPostRequest extends Request
{
    // Name the Error Message Bag
    protected $errorBag = 'register';

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username' => 'required|alpha_num|min:4|max:16|unique:users,username,:username',
            'password' => 'required|alpha_num|min:4|max:16',
            'reTypePass' => 'required|same:password',
            'firstName' => 'required|min:2|max:32',
            'lastName' => 'required|min:2|max:32',
            'email' => 'required|email',
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'Username is required',
            'username.alpha_num' => 'Username must contain only letters and digits',
            'username.min' => 'Username must contain a minimum of 6 characters',
            'username.max' => 'Username must contain a maximum of 16 characters',
            'username.unique' => 'User with the same Username already exists, choose another one',
            'password.required' => 'Password is required',
            'password.alpha_num' => 'Password must contain only letters and digits',
            'password.min' => 'Password must contain a minimum of 6 characters',
            'password.max' => 'Password must contain a maximum of 16 characters',
            'reTypePass.required' => 'Re-typed password is required',
            'reTypePass.same' => 'Re-typed password must match the Password',
            'firstName.required' => 'First Name is required',
            'firstName.min' => 'First Name must contain a minimum of 2 characters',
            'firstName.max' => 'First Name must contain a maximum of 32 characters',
            'lastName.required' => 'Last Name is required',
            'lastName.min' => 'Last Name must contain a minimum of 2 characters',
            'lastName.max' => 'Last Name must contain a maximum of 32 characters',
            'email.required' => 'Email is required',
            'email.email' => 'Email is not valid',
        ];
    }
}
