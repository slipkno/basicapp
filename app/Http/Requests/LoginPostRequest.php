<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LoginPostRequest extends Request
{
    // Name the Error Message Bag
    protected $errorBag = 'login';

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username' => 'required|alpha_num|min:4|max:16',
            'password' => 'required|alpha_num|min:4|max:16',
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'Username is required',
            'username.alpha_num' => 'Username must contain only letters and digits',
            'username.min' => 'Username must contain a minimum of 4 characters',
            'username.max' => 'Username must contain a maximum of 16 characters',
            'password.required' => 'Password is required',
            'password.alpha_num' => 'Password must contain only letters and digits',
            'password.min' => 'Password must contain a minimum of 6 characters',
            'password.max' => 'Password must contain a maximum of 16 characters',
        ];
    }
}
