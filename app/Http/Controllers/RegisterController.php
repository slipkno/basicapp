<?php

namespace App\Http\Controllers;

// Models
use App\Type;
use App\Role;
use App\User;
use App\RoleUser;

// Validation Requests
use App\Http\Requests\RegisterPostRequest;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    public function getRegister()
    {
        $types = Type::all();
        $roles = Role::all();

        return view('pages.register', [
            'types' => $types,
            'roles' => $roles,
        ]);
    }

    public function postRegister(RegisterPostRequest $request)
    {
        $data = $request->all();

        User::create([
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            'first_name' => $data['firstName'],
            'last_name' => $data['lastName'],
            'email' => $data['email'],
            'type_id' => $data['typeId'],
            'is_active' => $data['isActive'],
        ]);

        $user = User::where('username', $data['username'])->first();

        if (isset($data['register'])) {
            RoleUser::create([
                'user_id' => $user->id,
                'role_id' => $data['register'],
            ]);
        }

        if (isset($data['update'])) {
            RoleUser::create([
                'user_id' => $user->id,
                'role_id' => $data['update'],
            ]);
        }

        if (isset($data['block'])) {
            RoleUser::create([
                'user_id' => $user->id,
                'role_id' => $data['block'],
            ]);
        }

        Session::flash('message', 'You have successfully registered new User: ' . $user->username);
        return redirect('all-users');
    }
}
