<?php

namespace App\Http\Controllers;

// Models
use DB;
use App\Type;
use App\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function getAllUsers()
    {
        $paginator = User::getAllUsers()->paginate(7);
        $types = Type::all();

        return view('pages.allUsers', [
            'paginator' => $paginator,
            'types' => $types,
        ]);
    }

    public function postAllUsers(Request $request)
    {
        $data = $request->all();
        $user = User::find($data['user_id']);

        // Change Type
        if (isset($data['type_id'])) {
            $user->type_id = $data['type_id'];
            $user->save();
        }

        Session::flash('message', $user->username . ' type is changed');

        // Activate
        if (isset($_POST['activate'])) {
            $user->is_active = 1;
            $user->save();

            Session::flash('message', $user->username . ' is activated');
        }

        // Deactivate
        if (isset($_POST['deactivate'])) {
            $user->is_active = 0;
            $user->save();

            Session::flash('message', $user->username . ' is deactivated');
        }

        // Show Roles
        if (isset($_POST['roles'])) {
            $roles = null;

            $rolesSentence = $user->username . ' has no roles';

            $userRoles = $user->roles;

            if (!$userRoles->isEmpty()) {
                foreach ($userRoles as $role) {
                    $roles[] = $role->description;
                }

                $rolesSentence = implode(', ', $roles);
            }

            Session::flash('message', $rolesSentence);
        }

        return Redirect::route('all-users');
    }
}
