<?php

namespace App\Http\Controllers;

// Models
use App\Type;
use App\User;

// Validation Requests
use App\Http\Requests\LoginPostRequest;

use Hash;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    // Index page
    public function index()
    {
        return view('pages.login');
    }

    // Login ===========================================================================================================
    public function getLogin()
    {
        return view('pages.login');
    }

    public function postLogin(LoginPostRequest $request)
    {
        $data = $request->all();

        $user = User::where('username', $data['username'])->first();

        if (!$user) {
            $err = ['invalidUsernameOrPass' => 'Invalid Username or Password'];

            return redirect()->back()
                ->withErrors($err, 'login')
                ->withInput();

        } else {
            if (Hash::check($data['password'], $user->password)) {
                if ($user->is_active == 0) {
                    $err = ['blocked' => 'You have been blocked from our system'];

                    return redirect()->back()
                        ->withErrors($err, 'login')
                        ->withInput();
                }

                $type = Type::find($user->type_id)->name;

                $roles = null;

                foreach ($user->roles as $role) {
                    $roles[] = $role->name;
                }

                $data = [
                    'id' => $user->id,
                    'name' => $user->first_name,
                    'type' => $type,
                    'roles' => $roles,
                ];

                Session::set('user', $data);

                return redirect('all-users');

            } else {
                $err = ['invalidUsernameOrPassword' => 'Invalid Username or Password'];

                return redirect()->back()
                    ->withErrors($err, 'login')
                    ->withInput();
            }
        }
    }

    // Logout ==========================================================================================================
    public function logout()
    {
        Session::forget('user');
        return redirect('/');
    }
}
