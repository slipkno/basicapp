<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Root
Route::get('/', 'LoginController@index');

// Authentication ======================================================================================================

Route::get('login',
    ['as' => 'login', 'uses' => 'LoginController@getLogin']);
Route::post('login',
    ['as' => 'login', 'uses' => 'LoginController@postLogin']);
Route::get('logout', 'LoginController@logout');

// Users ===============================================================================================================

Route::group(['middleware' => 'authUsers'], function () {

    Route::get('all-users',
        ['as' => 'all-users', 'uses' => 'PagesController@getAllUsers']);
    Route::post('all-users',
        ['as' => 'all-users', 'uses' => 'PagesController@postAllUsers']);

    Route::get('register-new-user',
        ['as' => 'register-new-user', 'uses' => 'RegisterController@getRegister']);
    Route::post('register-new-user',
        ['as' => 'register-new-user', 'uses' => 'RegisterController@postRegister']);

});
