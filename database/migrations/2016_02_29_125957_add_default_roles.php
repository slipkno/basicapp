<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultRoles extends Migration
{
    /**
     * @var int
     */
    public $isActive;
    /**
     * @var string
     */
    public $dbTable;

    public function __construct()
    {
        // 0 == Inactive
        // 1 == Active
        $this->isActive = 1;

        $this->dbTable = 'roles';
    }

    public function up()
    {
        if ($this->isActive == 1) {
            DB::table($this->dbTable)->insert([
                'name' => 'register',
                'description' => 'Can register new Users',
            ]);

            DB::table($this->dbTable)->insert([
                'name' => 'update',
                'description' => 'Can update Users type',
            ]);

            DB::table($this->dbTable)->insert([
                'name' => 'block',
                'description' => 'Can block Users',
            ]);
        }
    }

    public function down()
    {
        if ($this->isActive == 1) {
            DB::table($this->dbTable)->where('id', '=', 1)->delete();
            DB::table($this->dbTable)->where('id', '=', 2)->delete();
            DB::table($this->dbTable)->where('id', '=', 3)->delete();
        }
    }
}
