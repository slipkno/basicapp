<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultUserTypes extends Migration
{
    /**
     * @var int
     */
    public $isActive;
    /**
     * @var string
     */
    public $dbTable;

    public function __construct()
    {
        // 0 == Inactive
        // 1 == Active
        $this->isActive = 1;

        $this->dbTable = 'types';
    }

    public function up()
    {
        if ($this->isActive == 1) {
            DB::table($this->dbTable)->insert([
                'name' => 'Master',
            ]);

            DB::table($this->dbTable)->insert([
                'name' => 'Admin',
            ]);

            DB::table($this->dbTable)->insert([
                'name' => 'Moderator',
            ]);
        }
    }

    public function down()
    {
        if ($this->isActive == 1) {
            DB::table($this->dbTable)->where('id', '=', 1)->delete();
            DB::table($this->dbTable)->where('id', '=', 2)->delete();
            DB::table($this->dbTable)->where('id', '=', 3)->delete();
        }
    }
}
