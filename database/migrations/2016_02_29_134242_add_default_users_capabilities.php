<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultUsersCapabilities extends Migration
{
    /**
     * @var int
     */
    public $isActive;
    /**
     * @var string
     */
    public $dbTable;

    public function __construct()
    {
        // 0 == Inactive
        // 1 == Active
        $this->isActive = 1;

        $this->dbTable = 'role_user';
    }

    public function up()
    {
        if ($this->isActive == 1) {
            // Masters =====================================================================================================

            // User 1
            DB::table($this->dbTable)->insert(['user_id' => 1, 'role_id' => 1]); // register
            DB::table($this->dbTable)->insert(['user_id' => 1, 'role_id' => 2]); // update
            DB::table($this->dbTable)->insert(['user_id' => 1, 'role_id' => 3]); // block

            // User 2
            DB::table($this->dbTable)->insert(['user_id' => 2, 'role_id' => 1]); // register
            DB::table($this->dbTable)->insert(['user_id' => 2, 'role_id' => 2]); // update
            DB::table($this->dbTable)->insert(['user_id' => 2, 'role_id' => 3]); // block

            // User 3
            DB::table($this->dbTable)->insert(['user_id' => 3, 'role_id' => 1]); // register
            DB::table($this->dbTable)->insert(['user_id' => 3, 'role_id' => 2]); // update
            DB::table($this->dbTable)->insert(['user_id' => 3, 'role_id' => 3]); // block

            // End Masters =================================================================================================

            // Admins ======================================================================================================

            // User 4
            DB::table($this->dbTable)->insert(['user_id' => 4, 'role_id' => 2]); // update
            DB::table($this->dbTable)->insert(['user_id' => 4, 'role_id' => 3]); // block

            // User 5

            DB::table($this->dbTable)->insert(['user_id' => 5, 'role_id' => 2]); // update
            DB::table($this->dbTable)->insert(['user_id' => 5, 'role_id' => 3]); // block

            // User 6

            DB::table($this->dbTable)->insert(['user_id' => 6, 'role_id' => 2]); // update
            DB::table($this->dbTable)->insert(['user_id' => 6, 'role_id' => 3]); // block

            // End Admins ==================================================================================================

            // Moderators ==================================================================================================

            // User 7

            DB::table($this->dbTable)->insert(['user_id' => 7, 'role_id' => 2]); // update

            // User 8

            DB::table($this->dbTable)->insert(['user_id' => 8, 'role_id' => 2]); // update

            // User 9

            DB::table($this->dbTable)->insert(['user_id' => 9, 'role_id' => 2]); // update

            // User 10

            DB::table($this->dbTable)->insert(['user_id' => 10, 'role_id' => 2]); // update

            // End Moderators ==============================================================================================
        }
    }

    public function down()
    {
        if ($this->isActive == 1) {
            // User 1

            DB::table($this->dbTable)->where('user_id', '=', 1)->where('role_id', '=', 1)->delete(); // register
            DB::table($this->dbTable)->where('user_id', '=', 1)->where('role_id', '=', 2)->delete(); // update
            DB::table($this->dbTable)->where('user_id', '=', 1)->where('role_id', '=', 3)->delete(); // block

            // User 2

            DB::table($this->dbTable)->where('user_id', '=', 2)->where('role_id', '=', 1)->delete(); // register
            DB::table($this->dbTable)->where('user_id', '=', 2)->where('role_id', '=', 2)->delete(); // update
            DB::table($this->dbTable)->where('user_id', '=', 2)->where('role_id', '=', 3)->delete(); // block

            // User 3

            DB::table($this->dbTable)->where('user_id', '=', 3)->where('role_id', '=', 1)->delete(); // register
            DB::table($this->dbTable)->where('user_id', '=', 3)->where('role_id', '=', 2)->delete(); // update
            DB::table($this->dbTable)->where('user_id', '=', 3)->where('role_id', '=', 3)->delete(); // block

            // User 4

            DB::table($this->dbTable)->where('user_id', '=', 4)->where('role_id', '=', 2)->delete(); // update
            DB::table($this->dbTable)->where('user_id', '=', 4)->where('role_id', '=', 3)->delete(); // block

            // User 5

            DB::table($this->dbTable)->where('user_id', '=', 5)->where('role_id', '=', 2)->delete(); // update
            DB::table($this->dbTable)->where('user_id', '=', 5)->where('role_id', '=', 3)->delete(); // block

            // User 6

            DB::table($this->dbTable)->where('user_id', '=', 6)->where('role_id', '=', 2)->delete(); // update
            DB::table($this->dbTable)->where('user_id', '=', 6)->where('role_id', '=', 3)->delete(); // block

            // User 7

            DB::table($this->dbTable)->where('user_id', '=', 7)->where('role_id', '=', 2)->delete(); // update

            // User 8

            DB::table($this->dbTable)->where('user_id', '=', 8)->where('role_id', '=', 2)->delete(); // update

            // User 9

            DB::table($this->dbTable)->where('user_id', '=', 9)->where('role_id', '=', 2)->delete(); // update

            // User 10

            DB::table($this->dbTable)->where('user_id', '=', 10)->where('role_id', '=', 2)->delete(); // update
        }
    }
}
