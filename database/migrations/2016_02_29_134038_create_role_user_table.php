<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleUserTable extends Migration
{
    /**
     * @var int
     */
    public $isActive;
    /**
     * @var string
     */
    public $dbTable;

    public function __construct()
    {
        // 0 == Inactive
        // 1 == Active
        $this->isActive = 1;

        $this->dbTable = 'role_user';
    }

    public function up()
    {
        if ($this->isActive == 1) {
            Schema::create($this->dbTable, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->integer('role_id')->unsigned();
            });
        }
    }

    public function down()
    {
        if ($this->isActive == 1) {
            Schema::drop($this->dbTable);
        }
    }
}
