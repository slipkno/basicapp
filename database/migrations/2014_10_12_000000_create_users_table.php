<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * @var int
     */
    public $isActive;
    /**
     * @var string
     */
    public $dbTable;

    public function __construct()
    {
        // 0 == Inactive
        // 1 == Active
        $this->isActive = 1;

        $this->dbTable = 'users';
    }

    public function up()
    {
        if ($this->isActive == 1) {
            Schema::create($this->dbTable, function (Blueprint $table) {
                $table->increments('id');
                $table->string('username')->unique();
                $table->string('password', 60);
                $table->string('first_name');
                $table->string('last_name');
                $table->string('email');
                $table->tinyInteger('type_id');
                $table->tinyInteger('is_active');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        if ($this->isActive == 1) {
            Schema::drop($this->dbTable);
        }
    }
}
