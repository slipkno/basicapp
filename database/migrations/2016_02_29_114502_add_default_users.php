<?php

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultUsers extends Migration
{
    /**
     * @var int
     */
    public $isActive;
    /**
     * @var string
     */
    public $dbTable;

    public function __construct()
    {
        // 0 == Inactive
        // 1 == Active
        $this->isActive = 1;

        $this->dbTable = 'users';
    }

    public function up()
    {
        if ($this->isActive == 1) {
            DB::table($this->dbTable)->insert([
                'username' => 'user1',
                'password' => '$2y$10$2AU.uNuNOD0wvN02F4c8Sux3Fpyy6xZWTRjDUKL86bCONwzgn.unm',
                'first_name' => 'Vladimir',
                'last_name' => 'Ivanov',
                'email' => 'email@email.bg',
                'type_id' => 1,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            DB::table($this->dbTable)->insert([
                'username' => 'user2',
                'password' => '$2y$10$y31J9ZXveohg8ugvR56EbuvkIijchQy7XTVabNn7qh5oSB0fVsIMe',
                'first_name' => 'Stefan',
                'last_name' => 'Petrov',
                'email' => 'email@email.bg',
                'type_id' => 1,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            DB::table($this->dbTable)->insert([
                'username' => 'user3',
                'password' => '$2y$10$sBpV8QLwq1/VKiWYUWePLOKPKk.4h9NJg4y5ynpGgWMPqXZdwAEq6',
                'first_name' => 'Ivan',
                'last_name' => 'Zhelyazkov',
                'email' => 'email@email.bg',
                'type_id' => 1,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            DB::table($this->dbTable)->insert([
                'username' => 'user4',
                'password' => '$2y$10$TlNHtFR2T.yiV696QOCtU.fXrkzamFmZkZc5HKebXJi/7da5Ox1H6',
                'first_name' => 'Peter',
                'last_name' => 'Genchev',
                'email' => 'email@email.bg',
                'type_id' => 2,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            DB::table($this->dbTable)->insert([
                'username' => 'user5',
                'password' => '$2y$10$LxqvY6XabEJT0TjdU4RT0uez4I0WBhxK.Xluky.9T9LkQq8MUbmrS',
                'first_name' => 'Genadi',
                'last_name' => 'Vasilev',
                'email' => 'email@email.bg',
                'type_id' => 2,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            DB::table($this->dbTable)->insert([
                'username' => 'user6',
                'password' => '$2y$10$3Vf0Lg/vns8qYdR/kvk4fe.iCOZXs8BBLA8aeE7HVxac6u1EzrSZe',
                'first_name' => 'Zdravko',
                'last_name' => 'Dimitrov',
                'email' => 'email@email.bg',
                'type_id' => 2,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            DB::table($this->dbTable)->insert([
                'username' => 'user7',
                'password' => '$2y$10$M4j0LBToZtlV7zc5BaL6e.9UcYDtzzZvbWjYp5opRYfgO0rdkMXxu',
                'first_name' => 'Kamen',
                'last_name' => 'Kremenliev',
                'email' => 'email@email.bg',
                'type_id' => 3,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            DB::table($this->dbTable)->insert([
                'username' => 'user8',
                'password' => '$2y$10$wp40KAj8ulR8jvHt9aVOIunHv2uQoY7QdTuEXQxdoW3p0awaBj7wS',
                'first_name' => 'Pencho',
                'last_name' => 'Totev',
                'email' => 'email@email.bg',
                'type_id' => 3,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            DB::table($this->dbTable)->insert([
                'username' => 'user9',
                'password' => '$2y$10$UMWy3yzge52fU5SEaUAseO3HaYeQYgEfKMuokAFj3qSp1KHxEaxfy',
                'first_name' => 'Galin',
                'last_name' => 'Iliev ',
                'email' => 'email@email.bg',
                'type_id' => 3,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            DB::table($this->dbTable)->insert([
                'username' => 'user10',
                'password' => '$2y$10$fSTzIOpIxRVa49oPIAFEfOqMYYhlIwvJQLeKaWAhJNEZS.HWYsPBq',
                'first_name' => 'Stefan',
                'last_name' => 'Filev',
                'email' => 'email@email.bg',
                'type_id' => 3,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }

    public function down()
    {
        if ($this->isActive == 1) {
            DB::table($this->dbTable)->where('id', '=', 1)->delete();
            DB::table($this->dbTable)->where('id', '=', 2)->delete();
            DB::table($this->dbTable)->where('id', '=', 3)->delete();
            DB::table($this->dbTable)->where('id', '=', 4)->delete();
            DB::table($this->dbTable)->where('id', '=', 5)->delete();
            DB::table($this->dbTable)->where('id', '=', 6)->delete();
            DB::table($this->dbTable)->where('id', '=', 7)->delete();
            DB::table($this->dbTable)->where('id', '=', 8)->delete();
            DB::table($this->dbTable)->where('id', '=', 9)->delete();
            DB::table($this->dbTable)->where('id', '=', 10)->delete();
        }
    }
}
