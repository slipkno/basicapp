<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTypesTable extends Migration
{
    /**
     * @var int
     */
    public $isActive;
    /**
     * @var string
     */
    public $dbTable;

    public function __construct()
    {
        // 0 == Inactive
        // 1 == Active
        $this->isActive = 1;

        $this->dbTable = 'types';
    }

    public function up()
    {
        if ($this->isActive == 1) {
            Schema::create($this->dbTable, function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
            });
        }
    }

    public function down()
    {
        if ($this->isActive == 1) {
            Schema::drop($this->dbTable);
        }
    }
}
